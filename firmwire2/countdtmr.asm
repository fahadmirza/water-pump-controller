
_TriggerCountdTimer:

;countdtmr.c,4 :: 		unsigned short TriggerCountdTimer(CountdTmrStruct *pStruct) {
;countdtmr.c,6 :: 		if(!pStruct->ssDiv) {
	MOVLW      3
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer0
;countdtmr.c,7 :: 		if(!pStruct->ss) {
	MOVLW      2
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer1
;countdtmr.c,8 :: 		if(!pStruct->mn) {
	INCF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer2
;countdtmr.c,9 :: 		if(pStruct->hh > 0) pStruct->hh--;
	MOVF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_TriggerCountdTimer3
	MOVF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	DECF       R0+0, 1
	MOVF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
L_TriggerCountdTimer3:
;countdtmr.c,10 :: 		if(pStruct->hh==0 && pStruct->mn==0 && pStruct->ss==0) {
	MOVF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer6
	INCF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer6
	MOVLW      2
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_TriggerCountdTimer6
L__TriggerCountdTimer10:
;countdtmr.c,11 :: 		pStruct->tmrOn=0;
	MOVLW      4
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	BCF        INDF+0, 0
;countdtmr.c,12 :: 		return 0;
	CLRF       R0+0
	RETURN
;countdtmr.c,13 :: 		}
L_TriggerCountdTimer6:
;countdtmr.c,14 :: 		pStruct->mn=59;
	INCF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVLW      59
	MOVWF      INDF+0
;countdtmr.c,15 :: 		} else pStruct->mn--;
	GOTO       L_TriggerCountdTimer7
L_TriggerCountdTimer2:
	INCF       FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
L_TriggerCountdTimer7:
;countdtmr.c,16 :: 		pStruct->ss=59;
	MOVLW      2
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVLW      59
	MOVWF      INDF+0
;countdtmr.c,17 :: 		} else pStruct->ss--;
	GOTO       L_TriggerCountdTimer8
L_TriggerCountdTimer1:
	MOVLW      2
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
L_TriggerCountdTimer8:
;countdtmr.c,18 :: 		pStruct->ssDiv = 9;
	MOVLW      3
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVLW      9
	MOVWF      INDF+0
;countdtmr.c,19 :: 		} else pStruct->ssDiv--;
	GOTO       L_TriggerCountdTimer9
L_TriggerCountdTimer0:
	MOVLW      3
	ADDWF      FARG_TriggerCountdTimer_pStruct+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
L_TriggerCountdTimer9:
;countdtmr.c,21 :: 		return 1;
	MOVLW      1
	MOVWF      R0+0
;countdtmr.c,23 :: 		}
	RETURN
; end of _TriggerCountdTimer

_SetCountdTimer:

;countdtmr.c,25 :: 		void SetCountdTimer(unsigned short hh,unsigned short mn,unsigned short ss,unsigned short ssDiv,CountdTmrStruct *pStruct) {
;countdtmr.c,27 :: 		pStruct->hh = hh;
	MOVF       FARG_SetCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       FARG_SetCountdTimer_hh+0, 0
	MOVWF      INDF+0
;countdtmr.c,28 :: 		pStruct->mn = mn;
	INCF       FARG_SetCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       FARG_SetCountdTimer_mn+0, 0
	MOVWF      INDF+0
;countdtmr.c,29 :: 		pStruct->ss = ss;
	MOVLW      2
	ADDWF      FARG_SetCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       FARG_SetCountdTimer_ss+0, 0
	MOVWF      INDF+0
;countdtmr.c,30 :: 		pStruct->ssDiv=ssDiv;
	MOVLW      3
	ADDWF      FARG_SetCountdTimer_pStruct+0, 0
	MOVWF      FSR
	MOVF       FARG_SetCountdTimer_ssDiv+0, 0
	MOVWF      INDF+0
;countdtmr.c,32 :: 		}
	RETURN
; end of _SetCountdTimer

_StartCountdTimer:

;countdtmr.c,34 :: 		void StartCountdTimer(CountdTmrStruct *pStruct) {
;countdtmr.c,36 :: 		pStruct->tmrOn = 1;
	MOVLW      4
	ADDWF      FARG_StartCountdTimer_pStruct+0, 0
	MOVWF      FSR
	BSF        INDF+0, 0
;countdtmr.c,38 :: 		}
	RETURN
; end of _StartCountdTimer

_StopCountdTimer:

;countdtmr.c,40 :: 		void StopCountdTimer(CountdTmrStruct *pStruct) {
;countdtmr.c,42 :: 		pStruct->tmrOn = 0;
	MOVLW      4
	ADDWF      FARG_StopCountdTimer_pStruct+0, 0
	MOVWF      FSR
	BCF        INDF+0, 0
;countdtmr.c,44 :: 		}
	RETURN
; end of _StopCountdTimer
