#include "countdtmr.h"


unsigned short TriggerCountdTimer(CountdTmrStruct *pStruct) {

         if(!pStruct->ssDiv) {
            if(!pStruct->ss) {
               if(!pStruct->mn) {
                  if(pStruct->hh > 0) pStruct->hh--;
                  if(pStruct->hh==0 && pStruct->mn==0 && pStruct->ss==0) {
                     pStruct->tmrOn=0;
                     return 0;
                  }
                  pStruct->mn=59;
               } else pStruct->mn--;
               pStruct->ss=59;
            } else pStruct->ss--;
            pStruct->ssDiv = 9;
         } else pStruct->ssDiv--;
         
         return 1;
         
}

void SetCountdTimer(unsigned short hh,unsigned short mn,unsigned short ss,unsigned short ssDiv,CountdTmrStruct *pStruct) {

     pStruct->hh = hh;
     pStruct->mn = mn;
     pStruct->ss = ss;
     pStruct->ssDiv=ssDiv;
     
}

void StartCountdTimer(CountdTmrStruct *pStruct) {

     pStruct->tmrOn = 1;
     
}

void StopCountdTimer(CountdTmrStruct *pStruct) {

     pStruct->tmrOn = 0;

}