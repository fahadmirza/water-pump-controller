#include "main.h"

sbit SNS_DRV_STB at PORTB.B2;
sbit SNS_DRV_STB_Dir at TRISB.B2;
sbit BLINK_LED at PORTB.B3;
sbit BLINK_LED_Dir at TRISB.B3;

sbit SW_MAN_LMOTOR at PORTA.B3;
sbit SW_MAN_DMOTOR at PORTA.B5;
sbit SW_MAN_LMOTOR_Dir at TRISA.B3;
sbit SW_MAN_DMOTOR_Dir at TRISA.B5;

/*sbit SW1_RUNTIME_LMOTOR at PORTC.B4;
sbit SW2_RUNTIME_LMOTOR at PORTC.B5;
sbit SW1_RUNTIME_DMOTOR at PORTC.B6;
sbit SW2_RUNTIME_DMOTOR at PORTC.B7;

sbit SW1_RUNTIME_LMOTOR_Dir at TRISC.B4;
sbit SW2_RUNTIME_LMOTOR_Dir at TRISC.B5;
sbit SW1_RUNTIME_DMOTOR_Dir at TRISC.B6;
sbit SW2_RUNTIME_DMOTOR_Dir at TRISC.B7;*/


sbit RLY_LMOTOR_PRI at PORTC.B0;
sbit RLY_LMOTOR_SEC at PORTC.B1;
sbit RLY_DMOTOR2    at PORTC.B2;
sbit RLY_DMOTOR     at PORTC.B3;

sbit RLY_LMOTOR_PRI_Dir at TRISC.B0;
sbit RLY_LMOTOR_SEC_Dir at TRISC.B1;
sbit RLY_DMOTOR2_Dir    at TRISC.B2;
sbit RLY_DMOTOR_Dir     at TRISC.B3;



struct SensorState {
       unsigned RESERVED2 : 1;
       unsigned RESERVED1 : 1;
       unsigned uTankHigh : 1;
       unsigned uTankLow  : 1;
       unsigned lTankHigh : 1;
       unsigned lTankmid  : 1;
       unsigned lTanklow  : 1;
       unsigned line      : 1;
} sensorStates;

typedef struct {
       unsigned RESERVED2    : 1;
       unsigned lineState    : 1;
       unsigned uTank        : 1;
       unsigned lTankPartial : 1;
       unsigned lTank        : 1;
       unsigned primMotor    : 1;
       unsigned altMotor     : 1;
       unsigned lineMotor    : 1;
} SystemStates;

SystemStates  sysStates;

typedef struct {
        unsigned short hh;
        unsigned short mn;
        unsigned short ss;
        unsigned RESERVED7 : 1;
        unsigned RESERVED6 : 1;
        unsigned RESERVED5 : 1;
        unsigned RESERVED4 : 1;
        unsigned RESERVED3 : 1;
        unsigned RESERVED2 : 1;
        unsigned RESERVED1 : 1;
        unsigned runFlag   : 1;
} StateTimer;

StateTimer lineRunTmr,lineRestTmr,lineCheckTmr;
StateTimer liftRunTmr,liftRestTmr;


#define _CAPTURE_SIG_FALL_EDGE 0x04
#define _CAPTURE_SIG_RISE_EDGE 0x05
#define _CAPTURE_SETTINGS_MASK 0xF0

#define _SIG_THRESH_LOW  100
#define _SIG_THRESH_HIGH 145

#define _CPU_FREQ 12000000

#define _STATE_LINE_MOTOR_RUN   1
#define _STATE_LINE_MOTOR_REST  2
#define _STATE_LINE_MOTOR_CHECK 3
#define _STATE_LINE_MOTOR_STOP  4

#define _STATE_LIFT_MOTOR_RUN  1
#define _STATE_LIFT_MOTOR_REST 2
#define _STATE_LIFT_MOTOR_STOP 3

#define _MUX_SEL_JMP_MAN_DMOTOR 6
#define _MUX_SEL_JMP_MAN_LMOTOR 7

volatile unsigned short myflag = 0;
//volatile unsigned char cnt = 0;
unsigned short lineState, liftState;


// Timer Codes
unsigned short ProcessTimer(StateTimer *pStateTimer) {

            if(!pStateTimer->ss) 
            {
               if(!pStateTimer->mn) 
               {
                  if(pStateTimer->hh > 0)
                  {
                     pStateTimer->hh--;
                     pStateTimer->mn=59;
                  }
                  if(pStateTimer->hh==0 && pStateTimer->mn==0 && pStateTimer->ss==0) 
                  {
                     pStateTimer->runFlag=0;
                     return 0;
                  }
               } 
               else
               {
                  pStateTimer->mn--;
                  pStateTimer->ss=59;
               }
            } 
            else 
               pStateTimer->ss--;
            
            return 1;
            
}

unsigned short IsTimerElapsed(StateTimer *pStateTimer) {

         return !pStateTimer->runFlag;
         
}

void StartTimer(unsigned short hh,unsigned short mn, unsigned short ss,StateTimer *pStateTimer) {
     pStateTimer->hh = hh;
     pStateTimer->mn = mn;
     pStateTimer->ss = ss;
     pStateTimer->runFlag = 1;
}
// Timer Codes

void interrupt() 
{
     
     if(INTCON & (1 << TMR0IF)) 
     {
        myflag=1;
        BLINK_LED = ~BLINK_LED;
        TMR0 = 0x61;
        INTCON &= ~(1 <<TMR0IF);
     }
     
}

void SetLineMotorState(unsigned short state) 
{
     lineState = state;
}



void SetLiftMotorState(unsigned short state) 
{
     liftState = state;
}


void ManageLiftMotorStates(SystemStates *pSysStates) {

     switch(liftState)
     {
     
      case _STATE_LIFT_MOTOR_RUN :
           /*******************************************************************/
           //Lcd_Out(1,1,"DRun  ");
           /*******************************************************************/
           RLY_LMOTOR_PRI = 1;

           if(pSysStates->uTank || !pSysStates->lTankPartial)
              SetLiftMotorState(_STATE_LIFT_MOTOR_STOP);
           if(IsTimerElapsed(&liftRunTmr)) {
              SetLiftMotorState(_STATE_LIFT_MOTOR_REST);
              StartTimer(0,30,0,&liftRestTmr);
           }
           break;

      case _STATE_LIFT_MOTOR_REST :
           /*******************************************************************/
           //Lcd_Out(1,1,"DRest ");
           /*******************************************************************/
           RLY_LMOTOR_PRI = 0;

           if(IsTimerElapsed(&liftRestTmr)) {
              SetLiftMotorState(_STATE_LIFT_MOTOR_RUN);
              StartTimer(0,210,0,&liftRunTmr);
           }
           break;

      case _STATE_LIFT_MOTOR_STOP :
           /*******************************************************************/
           //Lcd_Out(1,1,"DStop ");
           /*******************************************************************/
           RLY_LMOTOR_PRI = 0;

           if(!pSysStates->uTank && pSysStates->lTankPartial)
           {
              SetLiftMotorState(_STATE_LIFT_MOTOR_RUN);
              StartTimer(0,210,0,&liftRunTmr);
           }
           /*if(!SW_MAN_LMOTOR)
           {
              if(sensorStates.lTankMid)
              {
                 pSysStates->uTank=0;
              }
           }*/
           break;
     }
}

void ManageLineMotorStates(SystemStates *pSysStates) 
{

     switch(lineState)
     {
      
      case _STATE_LINE_MOTOR_RUN :
           /*******************************************************************/
           //Lcd_Out(1,1,"LRun  ");
           /*******************************************************************/
           RLY_DMOTOR = 1;

           if(!pSysStates->lineState)
           {
              SetLineMotorState(_STATE_LINE_MOTOR_REST);
              StartTimer(0,0,30,&lineRestTmr);
           }
           if(pSysStates->lTank)
              SetLineMotorState(_STATE_LINE_MOTOR_STOP);
           if(IsTimerElapsed(&lineRunTmr)) {
              SetLineMotorState(_STATE_LINE_MOTOR_REST);
              StartTimer(0,30,0,&lineRestTmr);
           }
           break;

      case _STATE_LINE_MOTOR_REST :
           /*******************************************************************/
           //Lcd_Out(1,1,"LRest ");
           /*******************************************************************/

           RLY_DMOTOR = 0;
           if(IsTimerElapsed(&lineRestTmr)) 
           {
              if(pSysStates->lTank) 
              {
                 SetLineMotorState(_STATE_LINE_MOTOR_STOP);
              } 
              else 
              {
                 SetLineMotorState(_STATE_LINE_MOTOR_CHECK);
                 StartTimer(0,1,0,&lineCheckTmr);
              }
           }
           break;
      
      case _STATE_LINE_MOTOR_STOP :
           /*******************************************************************/
           //Lcd_Out(1,1,"LStop ");
           /*******************************************************************/
           RLY_DMOTOR = 0;

           if(pSysStates->lTank==0)
           {
              SetLineMotorState(_STATE_LINE_MOTOR_CHECK);
              StartTimer(0,1,0,&lineCheckTmr);
              lineCheckTmr.mn = 1;
              lineCheckTmr.runFlag = 1;
           }
           /*if(!SW_MAN_DMOTOR)
           {
              if(!sensorStates.lTankHigh)
              {
                 pSysStates->lTank=0;
              }
           }*/
           break;
      
      case _STATE_LINE_MOTOR_CHECK :
           /*******************************************************************/
           //Lcd_Out(1,1,"LCheck");
           /*******************************************************************/
           RLY_DMOTOR = 1;

           if(IsTimerElapsed(&lineCheckTmr)) {
              if(pSysStates->lineState) 
              {
                 SetLineMotorState(_STATE_LINE_MOTOR_RUN);
                 StartTimer(0,210,0,&lineRunTmr);
              } 
              else 
              {
                 SetLineMotorState(_STATE_LINE_MOTOR_REST);
                 StartTimer(0,30,0,&lineRestTmr);
              }
           }
           break;
      
     }
}

void main() 
{
     Delay_ms(30000);
     ADCON1 = 0x07;                    // Select port(RA0-3,5) as Digital output
     OPTION_REG &= ~(1 << NOT_RBPU);   // PORTB pull-ups are enabled by individual port latch values
     TRISA &= 0xF8;                    // Set PORTA pins as input
     TRISA.B4 = 1;
     
     TRISB |= (1 << TRISB0) | (1 << TRISB1) | (1 << TRISB4) | (1 << TRISB5) | (1 << TRISB6) | (1 << TRISB7);// Set PORTB0,1,4-7 pin as input
     TRISC |= 0xF0;
     
     SNS_DRV_STB_Dir = 0;   // TRISB.B2
     SNS_DRV_STB = 0;       // PORTB.B2
     BLINK_LED_Dir = 0;     // TRISB.B3
     
     SW_MAN_DMOTOR_Dir = 1;       // TRISA.B5
     SW_MAN_LMOTOR_Dir = 1;       // TRISA.B3
     
     
     //Timer 0 configuration
     OPTION_REG &= ~(1 << T0SE);
     TMR0 = 0x61;
     INTCON |= 1 << TMR0IE;              // Timer 0 overflow interrupt enable
     INTCON |= (1 << GIE) | (1 << PEIE); // Global interrupt enable, Enable peripheral interrupt
     ////////////////////////////////////////////////////////////////////////////////////////////
     
     RLY_LMOTOR_PRI_Dir = 0;      // TRISC.B0
     RLY_LMOTOR_SEC_Dir = 0;      // TRISC.B1
     RLY_DMOTOR2_Dir = 0;         // TRISC.B2
     RLY_DMOTOR_Dir = 0;          // TRISC.B3

     SetLiftMotorState(_STATE_LIFT_MOTOR_STOP);  // _STATE_LIFT_MOTOR_STOP = 3, liftState = state;
     SetLineMotorState(_STATE_LINE_MOTOR_STOP);  // _STATE_LINE_MOTOR_STOP = 4, lineState = state;

     
     while(1)
     {
           //asm { CLRWDT; }
             if(myflag==1) 
             {
              sensorStates.uTanklow = PORTB.B0;
              sensorStates.uTankHigh = PORTB.B1;
              sensorStates.lTanklow = PORTB.B4;
              sensorStates.lTankMid = PORTB.B5;
              sensorStates.lTankHigh = PORTB.B6;
              sensorStates.line = PORTB.B7;


              
              sysStates.uTank = ((sensorStates.uTankLow && sensorStates.uTankHigh) || (sysStates.uTank && (sensorStates.uTankLow || sensorStates.uTankHigh)));
              sysStates.lTank = ((sensorStates.lTankMid && sensorStates.lTankHigh) || (sysStates.lTank && (sensorStates.lTankMid || sensorStates.lTankHigh)));
              sysStates.lTankPartial = ((sensorStates.lTankLow && sensorStates.lTankMid) || (sysStates.lTankPartial && (sensorStates.lTankLow || sensorStates.lTankMid)));
              sysStates.lineState = sensorStates.line;
              
              
              ProcessTimer(&lineRunTmr);
              ProcessTimer(&lineRestTmr);
              ProcessTimer(&lineCheckTmr);

              ProcessTimer(&liftRunTmr);
              ProcessTimer(&liftRestTmr);

           
              ManageLineMotorStates(&sysStates);
              ManageLiftMotorStates(&sysStates);

              myFlag=0;
             }
              
     }
     
}