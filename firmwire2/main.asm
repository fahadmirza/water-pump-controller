
_ProcessTimer:

;main.c,105 :: 		unsigned short ProcessTimer(StateTimer *pStateTimer) {
;main.c,107 :: 		if(!pStateTimer->ss)
	MOVLW      2
	ADDWF      FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_ProcessTimer0
;main.c,109 :: 		if(!pStateTimer->mn)
	INCF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_ProcessTimer1
;main.c,111 :: 		if(pStateTimer->hh > 0)
	MOVF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_ProcessTimer2
;main.c,113 :: 		pStateTimer->hh--;
	MOVF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	DECF       R0+0, 1
	MOVF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;main.c,114 :: 		pStateTimer->mn=59;
	INCF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVLW      59
	MOVWF      INDF+0
;main.c,115 :: 		}
L_ProcessTimer2:
;main.c,116 :: 		if(pStateTimer->hh==0 && pStateTimer->mn==0 && pStateTimer->ss==0)
	MOVF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_ProcessTimer5
	INCF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_ProcessTimer5
	MOVLW      2
	ADDWF      FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_ProcessTimer5
L__ProcessTimer66:
;main.c,118 :: 		pStateTimer->runFlag=0;
	MOVLW      3
	ADDWF      FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	BCF        INDF+0, 7
;main.c,119 :: 		return 0;
	CLRF       R0+0
	RETURN
;main.c,120 :: 		}
L_ProcessTimer5:
;main.c,121 :: 		}
	GOTO       L_ProcessTimer6
L_ProcessTimer1:
;main.c,124 :: 		pStateTimer->mn--;
	INCF       FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;main.c,125 :: 		pStateTimer->ss=59;
	MOVLW      2
	ADDWF      FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVLW      59
	MOVWF      INDF+0
;main.c,126 :: 		}
L_ProcessTimer6:
;main.c,127 :: 		}
	GOTO       L_ProcessTimer7
L_ProcessTimer0:
;main.c,129 :: 		pStateTimer->ss--;
	MOVLW      2
	ADDWF      FARG_ProcessTimer_pStateTimer+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
L_ProcessTimer7:
;main.c,131 :: 		return 1;
	MOVLW      1
	MOVWF      R0+0
;main.c,133 :: 		}
	RETURN
; end of _ProcessTimer

_IsTimerElapsed:

;main.c,135 :: 		unsigned short IsTimerElapsed(StateTimer *pStateTimer) {
;main.c,137 :: 		return !pStateTimer->runFlag;
	MOVLW      3
	ADDWF      FARG_IsTimerElapsed_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSC      R0+0, 7
	GOTO       L__IsTimerElapsed78
	BSF        3, 0
	GOTO       L__IsTimerElapsed79
L__IsTimerElapsed78:
	BCF        3, 0
L__IsTimerElapsed79:
	MOVLW      0
	BTFSC      3, 0
	MOVLW      1
	MOVWF      R0+0
;main.c,139 :: 		}
	RETURN
; end of _IsTimerElapsed

_StartTimer:

;main.c,141 :: 		void StartTimer(unsigned short hh,unsigned short mn, unsigned short ss,StateTimer *pStateTimer) {
;main.c,142 :: 		pStateTimer->hh = hh;
	MOVF       FARG_StartTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       FARG_StartTimer_hh+0, 0
	MOVWF      INDF+0
;main.c,143 :: 		pStateTimer->mn = mn;
	INCF       FARG_StartTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       FARG_StartTimer_mn+0, 0
	MOVWF      INDF+0
;main.c,144 :: 		pStateTimer->ss = ss;
	MOVLW      2
	ADDWF      FARG_StartTimer_pStateTimer+0, 0
	MOVWF      FSR
	MOVF       FARG_StartTimer_ss+0, 0
	MOVWF      INDF+0
;main.c,145 :: 		pStateTimer->runFlag = 1;
	MOVLW      3
	ADDWF      FARG_StartTimer_pStateTimer+0, 0
	MOVWF      FSR
	BSF        INDF+0, 7
;main.c,146 :: 		}
	RETURN
; end of _StartTimer

_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;main.c,149 :: 		void interrupt()
;main.c,152 :: 		if(INTCON & (1 << TMR0IF))
	BTFSS      INTCON+0, 2
	GOTO       L_interrupt8
;main.c,154 :: 		myflag=1;
	MOVLW      1
	MOVWF      _myflag+0
;main.c,155 :: 		BLINK_LED = ~BLINK_LED;
	MOVLW      8
	XORWF      PORTB+0, 1
;main.c,156 :: 		TMR0 = 0x61;
	MOVLW      97
	MOVWF      TMR0+0
;main.c,157 :: 		INTCON &= ~(1 <<TMR0IF);
	BCF        INTCON+0, 2
;main.c,158 :: 		}
L_interrupt8:
;main.c,160 :: 		}
L__interrupt80:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_SetLineMotorState:

;main.c,162 :: 		void SetLineMotorState(unsigned short state)
;main.c,164 :: 		lineState = state;
	MOVF       FARG_SetLineMotorState_state+0, 0
	MOVWF      _lineState+0
;main.c,165 :: 		}
	RETURN
; end of _SetLineMotorState

_SetLiftMotorState:

;main.c,169 :: 		void SetLiftMotorState(unsigned short state)
;main.c,171 :: 		liftState = state;
	MOVF       FARG_SetLiftMotorState_state+0, 0
	MOVWF      _liftState+0
;main.c,172 :: 		}
	RETURN
; end of _SetLiftMotorState

_ManageLiftMotorStates:

;main.c,175 :: 		void ManageLiftMotorStates(SystemStates *pSysStates) {
;main.c,177 :: 		switch(liftState)
	GOTO       L_ManageLiftMotorStates9
;main.c,180 :: 		case _STATE_LIFT_MOTOR_RUN :
L_ManageLiftMotorStates11:
;main.c,184 :: 		RLY_LMOTOR_PRI = 1;
	BSF        PORTC+0, 0
;main.c,186 :: 		if(pSysStates->uTank || !pSysStates->lTankPartial)
	MOVF       FARG_ManageLiftMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSC      R0+0, 2
	GOTO       L__ManageLiftMotorStates68
	MOVF       FARG_ManageLiftMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSS      R0+0, 3
	GOTO       L__ManageLiftMotorStates68
	GOTO       L_ManageLiftMotorStates14
L__ManageLiftMotorStates68:
;main.c,187 :: 		SetLiftMotorState(_STATE_LIFT_MOTOR_STOP);
	MOVLW      3
	MOVWF      FARG_SetLiftMotorState_state+0
	CALL       _SetLiftMotorState+0
L_ManageLiftMotorStates14:
;main.c,188 :: 		if(IsTimerElapsed(&liftRunTmr)) {
	MOVLW      _liftRunTmr+0
	MOVWF      FARG_IsTimerElapsed_pStateTimer+0
	CALL       _IsTimerElapsed+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLiftMotorStates15
;main.c,189 :: 		SetLiftMotorState(_STATE_LIFT_MOTOR_REST);
	MOVLW      2
	MOVWF      FARG_SetLiftMotorState_state+0
	CALL       _SetLiftMotorState+0
;main.c,190 :: 		StartTimer(0,30,0,&liftRestTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      30
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _liftRestTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,191 :: 		}
L_ManageLiftMotorStates15:
;main.c,192 :: 		break;
	GOTO       L_ManageLiftMotorStates10
;main.c,194 :: 		case _STATE_LIFT_MOTOR_REST :
L_ManageLiftMotorStates16:
;main.c,198 :: 		RLY_LMOTOR_PRI = 0;
	BCF        PORTC+0, 0
;main.c,200 :: 		if(IsTimerElapsed(&liftRestTmr)) {
	MOVLW      _liftRestTmr+0
	MOVWF      FARG_IsTimerElapsed_pStateTimer+0
	CALL       _IsTimerElapsed+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLiftMotorStates17
;main.c,201 :: 		SetLiftMotorState(_STATE_LIFT_MOTOR_RUN);
	MOVLW      1
	MOVWF      FARG_SetLiftMotorState_state+0
	CALL       _SetLiftMotorState+0
;main.c,202 :: 		StartTimer(0,210,0,&liftRunTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      210
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _liftRunTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,203 :: 		}
L_ManageLiftMotorStates17:
;main.c,204 :: 		break;
	GOTO       L_ManageLiftMotorStates10
;main.c,206 :: 		case _STATE_LIFT_MOTOR_STOP :
L_ManageLiftMotorStates18:
;main.c,210 :: 		RLY_LMOTOR_PRI = 0;
	BCF        PORTC+0, 0
;main.c,212 :: 		if(!pSysStates->uTank && pSysStates->lTankPartial)
	MOVF       FARG_ManageLiftMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSC      R0+0, 2
	GOTO       L_ManageLiftMotorStates21
	MOVF       FARG_ManageLiftMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSS      R0+0, 3
	GOTO       L_ManageLiftMotorStates21
L__ManageLiftMotorStates67:
;main.c,214 :: 		SetLiftMotorState(_STATE_LIFT_MOTOR_RUN);
	MOVLW      1
	MOVWF      FARG_SetLiftMotorState_state+0
	CALL       _SetLiftMotorState+0
;main.c,215 :: 		StartTimer(0,210,0,&liftRunTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      210
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _liftRunTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,216 :: 		}
L_ManageLiftMotorStates21:
;main.c,224 :: 		break;
	GOTO       L_ManageLiftMotorStates10
;main.c,225 :: 		}
L_ManageLiftMotorStates9:
	MOVF       _liftState+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLiftMotorStates11
	MOVF       _liftState+0, 0
	XORLW      2
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLiftMotorStates16
	MOVF       _liftState+0, 0
	XORLW      3
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLiftMotorStates18
L_ManageLiftMotorStates10:
;main.c,226 :: 		}
	RETURN
; end of _ManageLiftMotorStates

_ManageLineMotorStates:

;main.c,228 :: 		void ManageLineMotorStates(SystemStates *pSysStates)
;main.c,231 :: 		switch(lineState)
	GOTO       L_ManageLineMotorStates22
;main.c,234 :: 		case _STATE_LINE_MOTOR_RUN :
L_ManageLineMotorStates24:
;main.c,238 :: 		RLY_DMOTOR = 1;
	BSF        PORTC+0, 3
;main.c,240 :: 		if(!pSysStates->lineState)
	MOVF       FARG_ManageLineMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSC      R0+0, 1
	GOTO       L_ManageLineMotorStates25
;main.c,242 :: 		SetLineMotorState(_STATE_LINE_MOTOR_REST);
	MOVLW      2
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,243 :: 		StartTimer(0,0,30,&lineRestTmr);
	CLRF       FARG_StartTimer_hh+0
	CLRF       FARG_StartTimer_mn+0
	MOVLW      30
	MOVWF      FARG_StartTimer_ss+0
	MOVLW      _lineRestTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,244 :: 		}
L_ManageLineMotorStates25:
;main.c,245 :: 		if(pSysStates->lTank)
	MOVF       FARG_ManageLineMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSS      R0+0, 4
	GOTO       L_ManageLineMotorStates26
;main.c,246 :: 		SetLineMotorState(_STATE_LINE_MOTOR_STOP);
	MOVLW      4
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
L_ManageLineMotorStates26:
;main.c,247 :: 		if(IsTimerElapsed(&lineRunTmr)) {
	MOVLW      _lineRunTmr+0
	MOVWF      FARG_IsTimerElapsed_pStateTimer+0
	CALL       _IsTimerElapsed+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates27
;main.c,248 :: 		SetLineMotorState(_STATE_LINE_MOTOR_REST);
	MOVLW      2
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,249 :: 		StartTimer(0,30,0,&lineRestTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      30
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _lineRestTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,250 :: 		}
L_ManageLineMotorStates27:
;main.c,251 :: 		break;
	GOTO       L_ManageLineMotorStates23
;main.c,253 :: 		case _STATE_LINE_MOTOR_REST :
L_ManageLineMotorStates28:
;main.c,258 :: 		RLY_DMOTOR = 0;
	BCF        PORTC+0, 3
;main.c,259 :: 		if(IsTimerElapsed(&lineRestTmr))
	MOVLW      _lineRestTmr+0
	MOVWF      FARG_IsTimerElapsed_pStateTimer+0
	CALL       _IsTimerElapsed+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates29
;main.c,261 :: 		if(pSysStates->lTank)
	MOVF       FARG_ManageLineMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSS      R0+0, 4
	GOTO       L_ManageLineMotorStates30
;main.c,263 :: 		SetLineMotorState(_STATE_LINE_MOTOR_STOP);
	MOVLW      4
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,264 :: 		}
	GOTO       L_ManageLineMotorStates31
L_ManageLineMotorStates30:
;main.c,267 :: 		SetLineMotorState(_STATE_LINE_MOTOR_CHECK);
	MOVLW      3
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,268 :: 		StartTimer(0,1,0,&lineCheckTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      1
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _lineCheckTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,269 :: 		}
L_ManageLineMotorStates31:
;main.c,270 :: 		}
L_ManageLineMotorStates29:
;main.c,271 :: 		break;
	GOTO       L_ManageLineMotorStates23
;main.c,273 :: 		case _STATE_LINE_MOTOR_STOP :
L_ManageLineMotorStates32:
;main.c,277 :: 		RLY_DMOTOR = 0;
	BCF        PORTC+0, 3
;main.c,279 :: 		if(pSysStates->lTank==0)
	MOVF       FARG_ManageLineMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSC      R0+0, 4
	GOTO       L_ManageLineMotorStates33
;main.c,281 :: 		SetLineMotorState(_STATE_LINE_MOTOR_CHECK);
	MOVLW      3
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,282 :: 		StartTimer(0,1,0,&lineCheckTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      1
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _lineCheckTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,283 :: 		lineCheckTmr.mn = 1;
	MOVLW      1
	MOVWF      _lineCheckTmr+1
;main.c,284 :: 		lineCheckTmr.runFlag = 1;
	BSF        _lineCheckTmr+3, 7
;main.c,285 :: 		}
L_ManageLineMotorStates33:
;main.c,293 :: 		break;
	GOTO       L_ManageLineMotorStates23
;main.c,295 :: 		case _STATE_LINE_MOTOR_CHECK :
L_ManageLineMotorStates34:
;main.c,299 :: 		RLY_DMOTOR = 1;
	BSF        PORTC+0, 3
;main.c,301 :: 		if(IsTimerElapsed(&lineCheckTmr)) {
	MOVLW      _lineCheckTmr+0
	MOVWF      FARG_IsTimerElapsed_pStateTimer+0
	CALL       _IsTimerElapsed+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates35
;main.c,302 :: 		if(pSysStates->lineState)
	MOVF       FARG_ManageLineMotorStates_pSysStates+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	BTFSS      R0+0, 1
	GOTO       L_ManageLineMotorStates36
;main.c,304 :: 		SetLineMotorState(_STATE_LINE_MOTOR_RUN);
	MOVLW      1
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,305 :: 		StartTimer(0,210,0,&lineRunTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      210
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _lineRunTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,306 :: 		}
	GOTO       L_ManageLineMotorStates37
L_ManageLineMotorStates36:
;main.c,309 :: 		SetLineMotorState(_STATE_LINE_MOTOR_REST);
	MOVLW      2
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,310 :: 		StartTimer(0,30,0,&lineRestTmr);
	CLRF       FARG_StartTimer_hh+0
	MOVLW      30
	MOVWF      FARG_StartTimer_mn+0
	CLRF       FARG_StartTimer_ss+0
	MOVLW      _lineRestTmr+0
	MOVWF      FARG_StartTimer_pStateTimer+0
	CALL       _StartTimer+0
;main.c,311 :: 		}
L_ManageLineMotorStates37:
;main.c,312 :: 		}
L_ManageLineMotorStates35:
;main.c,313 :: 		break;
	GOTO       L_ManageLineMotorStates23
;main.c,315 :: 		}
L_ManageLineMotorStates22:
	MOVF       _lineState+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates24
	MOVF       _lineState+0, 0
	XORLW      2
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates28
	MOVF       _lineState+0, 0
	XORLW      4
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates32
	MOVF       _lineState+0, 0
	XORLW      3
	BTFSC      STATUS+0, 2
	GOTO       L_ManageLineMotorStates34
L_ManageLineMotorStates23:
;main.c,316 :: 		}
	RETURN
; end of _ManageLineMotorStates

_main:

;main.c,318 :: 		void main()
;main.c,320 :: 		Delay_ms(30000);
	MOVLW      2
	MOVWF      R10+0
	MOVLW      201
	MOVWF      R11+0
	MOVLW      146
	MOVWF      R12+0
	MOVLW      234
	MOVWF      R13+0
L_main38:
	DECFSZ     R13+0, 1
	GOTO       L_main38
	DECFSZ     R12+0, 1
	GOTO       L_main38
	DECFSZ     R11+0, 1
	GOTO       L_main38
	DECFSZ     R10+0, 1
	GOTO       L_main38
	NOP
;main.c,321 :: 		ADCON1 = 0x07;                    // Select port(RA0-3,5) as Digital output
	MOVLW      7
	MOVWF      ADCON1+0
;main.c,322 :: 		OPTION_REG &= ~(1 << NOT_RBPU);   // PORTB pull-ups are enabled by individual port latch values
	MOVLW      127
	ANDWF      OPTION_REG+0, 1
;main.c,323 :: 		TRISA &= 0xF8;                    // Set PORTA pins as input
	MOVLW      248
	ANDWF      TRISA+0, 1
;main.c,324 :: 		TRISA.B4 = 1;
	BSF        TRISA+0, 4
;main.c,326 :: 		TRISB |= (1 << TRISB0) | (1 << TRISB1) | (1 << TRISB4) | (1 << TRISB5) | (1 << TRISB6) | (1 << TRISB7);// Set PORTB0,1,4-7 pin as input
	MOVLW      243
	IORWF      TRISB+0, 1
;main.c,327 :: 		TRISC |= 0xF0;
	MOVLW      240
	IORWF      TRISC+0, 1
;main.c,329 :: 		SNS_DRV_STB_Dir = 0;   // TRISB.B2
	BCF        TRISB+0, 2
;main.c,330 :: 		SNS_DRV_STB = 0;       // PORTB.B2
	BCF        PORTB+0, 2
;main.c,331 :: 		BLINK_LED_Dir = 0;     // TRISB.B3
	BCF        TRISB+0, 3
;main.c,333 :: 		SW_MAN_DMOTOR_Dir = 1;       // TRISA.B5
	BSF        TRISA+0, 5
;main.c,334 :: 		SW_MAN_LMOTOR_Dir = 1;       // TRISA.B3
	BSF        TRISA+0, 3
;main.c,338 :: 		OPTION_REG &= ~(1 << T0SE);
	BCF        OPTION_REG+0, 4
;main.c,339 :: 		TMR0 = 0x61;
	MOVLW      97
	MOVWF      TMR0+0
;main.c,340 :: 		INTCON |= 1 << TMR0IE;              // Timer 0 overflow interrupt enable
	BSF        INTCON+0, 5
;main.c,341 :: 		INTCON |= (1 << GIE) | (1 << PEIE); // Global interrupt enable, Enable peripheral interrupt
	MOVLW      192
	IORWF      INTCON+0, 1
;main.c,344 :: 		RLY_LMOTOR_PRI_Dir = 0;      // TRISC.B0
	BCF        TRISC+0, 0
;main.c,345 :: 		RLY_LMOTOR_SEC_Dir = 0;      // TRISC.B1
	BCF        TRISC+0, 1
;main.c,346 :: 		RLY_DMOTOR2_Dir = 0;         // TRISC.B2
	BCF        TRISC+0, 2
;main.c,347 :: 		RLY_DMOTOR_Dir = 0;          // TRISC.B3
	BCF        TRISC+0, 3
;main.c,349 :: 		SetLiftMotorState(_STATE_LIFT_MOTOR_STOP);  // _STATE_LIFT_MOTOR_STOP = 3, liftState = state;
	MOVLW      3
	MOVWF      FARG_SetLiftMotorState_state+0
	CALL       _SetLiftMotorState+0
;main.c,350 :: 		SetLineMotorState(_STATE_LINE_MOTOR_STOP);  // _STATE_LINE_MOTOR_STOP = 4, lineState = state;
	MOVLW      4
	MOVWF      FARG_SetLineMotorState_state+0
	CALL       _SetLineMotorState+0
;main.c,353 :: 		while(1)
L_main39:
;main.c,356 :: 		if(myflag==1)
	MOVF       _myflag+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main41
;main.c,358 :: 		sensorStates.uTanklow = PORTB.B0;
	BTFSC      PORTB+0, 0
	GOTO       L__main81
	BCF        _sensorStates+0, 3
	GOTO       L__main82
L__main81:
	BSF        _sensorStates+0, 3
L__main82:
;main.c,359 :: 		sensorStates.uTankHigh = PORTB.B1;
	BTFSC      PORTB+0, 1
	GOTO       L__main83
	BCF        _sensorStates+0, 2
	GOTO       L__main84
L__main83:
	BSF        _sensorStates+0, 2
L__main84:
;main.c,360 :: 		sensorStates.lTanklow = PORTB.B4;
	BTFSC      PORTB+0, 4
	GOTO       L__main85
	BCF        _sensorStates+0, 6
	GOTO       L__main86
L__main85:
	BSF        _sensorStates+0, 6
L__main86:
;main.c,361 :: 		sensorStates.lTankMid = PORTB.B5;
	BTFSC      PORTB+0, 5
	GOTO       L__main87
	BCF        _sensorStates+0, 5
	GOTO       L__main88
L__main87:
	BSF        _sensorStates+0, 5
L__main88:
;main.c,362 :: 		sensorStates.lTankHigh = PORTB.B6;
	BTFSC      PORTB+0, 6
	GOTO       L__main89
	BCF        _sensorStates+0, 4
	GOTO       L__main90
L__main89:
	BSF        _sensorStates+0, 4
L__main90:
;main.c,363 :: 		sensorStates.line = PORTB.B7;
	BTFSC      PORTB+0, 7
	GOTO       L__main91
	BCF        _sensorStates+0, 7
	GOTO       L__main92
L__main91:
	BSF        _sensorStates+0, 7
L__main92:
;main.c,367 :: 		sysStates.uTank = ((sensorStates.uTankLow && sensorStates.uTankHigh) || (sysStates.uTank && (sensorStates.uTankLow || sensorStates.uTankHigh)));
	BTFSS      _sensorStates+0, 3
	GOTO       L__main77
	BTFSS      _sensorStates+0, 2
	GOTO       L__main77
	GOTO       L_main49
L__main77:
	BTFSS      _sysStates+0, 2
	GOTO       L__main75
	BTFSC      _sensorStates+0, 3
	GOTO       L__main76
	BTFSC      _sensorStates+0, 2
	GOTO       L__main76
	GOTO       L__main75
L__main76:
	GOTO       L_main49
L__main75:
	CLRF       R0+0
	GOTO       L_main48
L_main49:
	MOVLW      1
	MOVWF      R0+0
L_main48:
	BTFSC      R0+0, 0
	GOTO       L__main93
	BCF        _sysStates+0, 2
	GOTO       L__main94
L__main93:
	BSF        _sysStates+0, 2
L__main94:
;main.c,368 :: 		sysStates.lTank = ((sensorStates.lTankMid && sensorStates.lTankHigh) || (sysStates.lTank && (sensorStates.lTankMid || sensorStates.lTankHigh)));
	BTFSS      _sensorStates+0, 5
	GOTO       L__main74
	BTFSS      _sensorStates+0, 4
	GOTO       L__main74
	GOTO       L_main57
L__main74:
	BTFSS      _sysStates+0, 4
	GOTO       L__main72
	BTFSC      _sensorStates+0, 5
	GOTO       L__main73
	BTFSC      _sensorStates+0, 4
	GOTO       L__main73
	GOTO       L__main72
L__main73:
	GOTO       L_main57
L__main72:
	CLRF       R0+0
	GOTO       L_main56
L_main57:
	MOVLW      1
	MOVWF      R0+0
L_main56:
	BTFSC      R0+0, 0
	GOTO       L__main95
	BCF        _sysStates+0, 4
	GOTO       L__main96
L__main95:
	BSF        _sysStates+0, 4
L__main96:
;main.c,369 :: 		sysStates.lTankPartial = ((sensorStates.lTankLow && sensorStates.lTankMid) || (sysStates.lTankPartial && (sensorStates.lTankLow || sensorStates.lTankMid)));
	BTFSS      _sensorStates+0, 6
	GOTO       L__main71
	BTFSS      _sensorStates+0, 5
	GOTO       L__main71
	GOTO       L_main65
L__main71:
	BTFSS      _sysStates+0, 3
	GOTO       L__main69
	BTFSC      _sensorStates+0, 6
	GOTO       L__main70
	BTFSC      _sensorStates+0, 5
	GOTO       L__main70
	GOTO       L__main69
L__main70:
	GOTO       L_main65
L__main69:
	CLRF       R0+0
	GOTO       L_main64
L_main65:
	MOVLW      1
	MOVWF      R0+0
L_main64:
	BTFSC      R0+0, 0
	GOTO       L__main97
	BCF        _sysStates+0, 3
	GOTO       L__main98
L__main97:
	BSF        _sysStates+0, 3
L__main98:
;main.c,370 :: 		sysStates.lineState = sensorStates.line;
	BTFSC      _sensorStates+0, 7
	GOTO       L__main99
	BCF        _sysStates+0, 1
	GOTO       L__main100
L__main99:
	BSF        _sysStates+0, 1
L__main100:
;main.c,373 :: 		ProcessTimer(&lineRunTmr);
	MOVLW      _lineRunTmr+0
	MOVWF      FARG_ProcessTimer_pStateTimer+0
	CALL       _ProcessTimer+0
;main.c,374 :: 		ProcessTimer(&lineRestTmr);
	MOVLW      _lineRestTmr+0
	MOVWF      FARG_ProcessTimer_pStateTimer+0
	CALL       _ProcessTimer+0
;main.c,375 :: 		ProcessTimer(&lineCheckTmr);
	MOVLW      _lineCheckTmr+0
	MOVWF      FARG_ProcessTimer_pStateTimer+0
	CALL       _ProcessTimer+0
;main.c,377 :: 		ProcessTimer(&liftRunTmr);
	MOVLW      _liftRunTmr+0
	MOVWF      FARG_ProcessTimer_pStateTimer+0
	CALL       _ProcessTimer+0
;main.c,378 :: 		ProcessTimer(&liftRestTmr);
	MOVLW      _liftRestTmr+0
	MOVWF      FARG_ProcessTimer_pStateTimer+0
	CALL       _ProcessTimer+0
;main.c,381 :: 		ManageLineMotorStates(&sysStates);
	MOVLW      _sysStates+0
	MOVWF      FARG_ManageLineMotorStates_pSysStates+0
	CALL       _ManageLineMotorStates+0
;main.c,382 :: 		ManageLiftMotorStates(&sysStates);
	MOVLW      _sysStates+0
	MOVWF      FARG_ManageLiftMotorStates_pSysStates+0
	CALL       _ManageLiftMotorStates+0
;main.c,384 :: 		myFlag=0;
	CLRF       _myflag+0
;main.c,385 :: 		}
L_main41:
;main.c,387 :: 		}
	GOTO       L_main39
;main.c,389 :: 		}
	GOTO       $+0
; end of _main
