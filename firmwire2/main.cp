#line 1 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
#line 1 "g:/fahad/pi-lab projects/water pump controller/firmwire2/main.h"
#line 19 "g:/fahad/pi-lab projects/water pump controller/firmwire2/main.h"
 code unsigned char debugMsg[][5] = {
 "LRUN",
 "LRST",
 "LSTP",
 "DCHK",
 "DRUN",
 "DRST",
 "DSTP",
 "UTL",
 "UTH",
 "LTL",
 "LTM",
 "LTH",
 "LINE",
 "N/A "
 };
#line 3 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
sbit SNS_DRV_STB at PORTB.B2;
sbit SNS_DRV_STB_Dir at TRISB.B2;
sbit BLINK_LED at PORTB.B3;
sbit BLINK_LED_Dir at TRISB.B3;

sbit SW_MAN_LMOTOR at PORTA.B3;
sbit SW_MAN_DMOTOR at PORTA.B5;
sbit SW_MAN_LMOTOR_Dir at TRISA.B3;
sbit SW_MAN_DMOTOR_Dir at TRISA.B5;
#line 24 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
sbit RLY_LMOTOR_PRI at PORTC.B0;
sbit RLY_LMOTOR_SEC at PORTC.B1;
sbit RLY_DMOTOR2 at PORTC.B2;
sbit RLY_DMOTOR at PORTC.B3;

sbit RLY_LMOTOR_PRI_Dir at TRISC.B0;
sbit RLY_LMOTOR_SEC_Dir at TRISC.B1;
sbit RLY_DMOTOR2_Dir at TRISC.B2;
sbit RLY_DMOTOR_Dir at TRISC.B3;



struct SensorState {
 unsigned RESERVED2 : 1;
 unsigned RESERVED1 : 1;
 unsigned uTankHigh : 1;
 unsigned uTankLow : 1;
 unsigned lTankHigh : 1;
 unsigned lTankmid : 1;
 unsigned lTanklow : 1;
 unsigned line : 1;
} sensorStates;

typedef struct {
 unsigned RESERVED2 : 1;
 unsigned lineState : 1;
 unsigned uTank : 1;
 unsigned lTankPartial : 1;
 unsigned lTank : 1;
 unsigned primMotor : 1;
 unsigned altMotor : 1;
 unsigned lineMotor : 1;
} SystemStates;

SystemStates sysStates;

typedef struct {
 unsigned short hh;
 unsigned short mn;
 unsigned short ss;
 unsigned RESERVED7 : 1;
 unsigned RESERVED6 : 1;
 unsigned RESERVED5 : 1;
 unsigned RESERVED4 : 1;
 unsigned RESERVED3 : 1;
 unsigned RESERVED2 : 1;
 unsigned RESERVED1 : 1;
 unsigned runFlag : 1;
} StateTimer;

StateTimer lineRunTmr,lineRestTmr,lineCheckTmr;
StateTimer liftRunTmr,liftRestTmr;
#line 99 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
volatile unsigned short myflag = 0;

unsigned short lineState, liftState;



unsigned short ProcessTimer(StateTimer *pStateTimer) {

 if(!pStateTimer->ss)
 {
 if(!pStateTimer->mn)
 {
 if(pStateTimer->hh > 0)
 {
 pStateTimer->hh--;
 pStateTimer->mn=59;
 }
 if(pStateTimer->hh==0 && pStateTimer->mn==0 && pStateTimer->ss==0)
 {
 pStateTimer->runFlag=0;
 return 0;
 }
 }
 else
 {
 pStateTimer->mn--;
 pStateTimer->ss=59;
 }
 }
 else
 pStateTimer->ss--;

 return 1;

}

unsigned short IsTimerElapsed(StateTimer *pStateTimer) {

 return !pStateTimer->runFlag;

}

void StartTimer(unsigned short hh,unsigned short mn, unsigned short ss,StateTimer *pStateTimer) {
 pStateTimer->hh = hh;
 pStateTimer->mn = mn;
 pStateTimer->ss = ss;
 pStateTimer->runFlag = 1;
}


void interrupt()
{

 if(INTCON & (1 << TMR0IF))
 {
 myflag=1;
 BLINK_LED = ~BLINK_LED;
 TMR0 = 0x61;
 INTCON &= ~(1 <<TMR0IF);
 }

}

void SetLineMotorState(unsigned short state)
{
 lineState = state;
}



void SetLiftMotorState(unsigned short state)
{
 liftState = state;
}


void ManageLiftMotorStates(SystemStates *pSysStates) {

 switch(liftState)
 {

 case  1  :



 RLY_LMOTOR_PRI = 1;

 if(pSysStates->uTank || !pSysStates->lTankPartial)
 SetLiftMotorState( 3 );
 if(IsTimerElapsed(&liftRunTmr)) {
 SetLiftMotorState( 2 );
 StartTimer(0,30,0,&liftRestTmr);
 }
 break;

 case  2  :



 RLY_LMOTOR_PRI = 0;

 if(IsTimerElapsed(&liftRestTmr)) {
 SetLiftMotorState( 1 );
 StartTimer(0,210,0,&liftRunTmr);
 }
 break;

 case  3  :



 RLY_LMOTOR_PRI = 0;

 if(!pSysStates->uTank && pSysStates->lTankPartial)
 {
 SetLiftMotorState( 1 );
 StartTimer(0,210,0,&liftRunTmr);
 }
#line 224 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
 break;
 }
}

void ManageLineMotorStates(SystemStates *pSysStates)
{

 switch(lineState)
 {

 case  1  :



 RLY_DMOTOR = 1;

 if(!pSysStates->lineState)
 {
 SetLineMotorState( 2 );
 StartTimer(0,0,30,&lineRestTmr);
 }
 if(pSysStates->lTank)
 SetLineMotorState( 4 );
 if(IsTimerElapsed(&lineRunTmr)) {
 SetLineMotorState( 2 );
 StartTimer(0,30,0,&lineRestTmr);
 }
 break;

 case  2  :




 RLY_DMOTOR = 0;
 if(IsTimerElapsed(&lineRestTmr))
 {
 if(pSysStates->lTank)
 {
 SetLineMotorState( 4 );
 }
 else
 {
 SetLineMotorState( 3 );
 StartTimer(0,1,0,&lineCheckTmr);
 }
 }
 break;

 case  4  :



 RLY_DMOTOR = 0;

 if(pSysStates->lTank==0)
 {
 SetLineMotorState( 3 );
 StartTimer(0,1,0,&lineCheckTmr);
 lineCheckTmr.mn = 1;
 lineCheckTmr.runFlag = 1;
 }
#line 293 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/main.c"
 break;

 case  3  :



 RLY_DMOTOR = 1;

 if(IsTimerElapsed(&lineCheckTmr)) {
 if(pSysStates->lineState)
 {
 SetLineMotorState( 1 );
 StartTimer(0,210,0,&lineRunTmr);
 }
 else
 {
 SetLineMotorState( 2 );
 StartTimer(0,30,0,&lineRestTmr);
 }
 }
 break;

 }
}

void main()
{
 Delay_ms(30000);
 ADCON1 = 0x07;
 OPTION_REG &= ~(1 << NOT_RBPU);
 TRISA &= 0xF8;
 TRISA.B4 = 1;

 TRISB |= (1 << TRISB0) | (1 << TRISB1) | (1 << TRISB4) | (1 << TRISB5) | (1 << TRISB6) | (1 << TRISB7);
 TRISC |= 0xF0;

 SNS_DRV_STB_Dir = 0;
 SNS_DRV_STB = 0;
 BLINK_LED_Dir = 0;

 SW_MAN_DMOTOR_Dir = 1;
 SW_MAN_LMOTOR_Dir = 1;



 OPTION_REG &= ~(1 << T0SE);
 TMR0 = 0x61;
 INTCON |= 1 << TMR0IE;
 INTCON |= (1 << GIE) | (1 << PEIE);


 RLY_LMOTOR_PRI_Dir = 0;
 RLY_LMOTOR_SEC_Dir = 0;
 RLY_DMOTOR2_Dir = 0;
 RLY_DMOTOR_Dir = 0;

 SetLiftMotorState( 3 );
 SetLineMotorState( 4 );


 while(1)
 {

 if(myflag==1)
 {
 sensorStates.uTanklow = PORTB.B0;
 sensorStates.uTankHigh = PORTB.B1;
 sensorStates.lTanklow = PORTB.B4;
 sensorStates.lTankMid = PORTB.B5;
 sensorStates.lTankHigh = PORTB.B6;
 sensorStates.line = PORTB.B7;



 sysStates.uTank = ((sensorStates.uTankLow && sensorStates.uTankHigh) || (sysStates.uTank && (sensorStates.uTankLow || sensorStates.uTankHigh)));
 sysStates.lTank = ((sensorStates.lTankMid && sensorStates.lTankHigh) || (sysStates.lTank && (sensorStates.lTankMid || sensorStates.lTankHigh)));
 sysStates.lTankPartial = ((sensorStates.lTankLow && sensorStates.lTankMid) || (sysStates.lTankPartial && (sensorStates.lTankLow || sensorStates.lTankMid)));
 sysStates.lineState = sensorStates.line;


 ProcessTimer(&lineRunTmr);
 ProcessTimer(&lineRestTmr);
 ProcessTimer(&lineCheckTmr);

 ProcessTimer(&liftRunTmr);
 ProcessTimer(&liftRestTmr);


 ManageLineMotorStates(&sysStates);
 ManageLiftMotorStates(&sysStates);

 myFlag=0;
 }

 }

}
