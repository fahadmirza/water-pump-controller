#ifndef _MAIN_H_
 #define _MAIN_H_
 
 #define _STATE_MSG_LRUN  0
 #define _STATE_MSG_LRST  1
 #define _STATE_MSG_LSTP  2
 #define _STATE_MSG_DCHK  3
 #define _STATE_MSG_DRUN  4
 #define _STATE_MSG_DRST  5
 #define _STATE_MSG_DSTP  6
 #define _STATE_MSG_UTL   7
 #define _STATE_MSG_UTH   8
 #define _STATE_MSG_LTL   9
 #define _STATE_MSG_LTM  10
 #define _STATE_MSG_LTH  11
 #define _STATE_MSG_LINE 12
 #define _STATE_MSG_NA  13
 
 code unsigned char debugMsg[][5] = {   
                                        "LRUN",
                                        "LRST",
                                        "LSTP",
                                        "DCHK",
                                        "DRUN",
                                        "DRST",
                                        "DSTP",
                                        "UTL",
                                        "UTH",
                                        "LTL",
                                        "LTM",
                                        "LTH",
                                        "LINE",
                                        "N/A "
                                    };
                                    
#endif