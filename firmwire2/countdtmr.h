#ifndef __COUNTDTMR_H__
 #define __COUNTDTMR_H__
 
 typedef struct {
         unsigned short hh;
         unsigned short mn;
         unsigned short ss;
         unsigned short ssDiv;
         tmrOn         : 1;
         reserved      : 7;
 } CountdTmrStruct;
 
 unsigned short TriggerCountdTimer(CountdTmrStruct *);
 void SetCountdTimer(unsigned short, unsigned short, unsigned short, unsigned short, CountdTmrStruct *);
 void StartCountdTimer(CountdTmrStruct *);
 void StopCountdTimer(CountdTmrStruct *);
 
 
#endif