#line 1 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/countdtmr.c"
#line 1 "g:/fahad/pi-lab projects/water pump controller/firmwire2/countdtmr.h"



 typedef struct {
 unsigned short hh;
 unsigned short mn;
 unsigned short ss;
 unsigned short ssDiv;
 tmrOn : 1;
 reserved : 7;
 } CountdTmrStruct;

 unsigned short TriggerCountdTimer(CountdTmrStruct *);
 void SetCountdTimer(unsigned short, unsigned short, unsigned short, unsigned short, CountdTmrStruct *);
 void StartCountdTimer(CountdTmrStruct *);
 void StopCountdTimer(CountdTmrStruct *);
#line 4 "G:/fahad/Pi-Lab projects/Water Pump Controller/firmwire2/countdtmr.c"
unsigned short TriggerCountdTimer(CountdTmrStruct *pStruct) {

 if(!pStruct->ssDiv) {
 if(!pStruct->ss) {
 if(!pStruct->mn) {
 if(pStruct->hh > 0) pStruct->hh--;
 if(pStruct->hh==0 && pStruct->mn==0 && pStruct->ss==0) {
 pStruct->tmrOn=0;
 return 0;
 }
 pStruct->mn=59;
 } else pStruct->mn--;
 pStruct->ss=59;
 } else pStruct->ss--;
 pStruct->ssDiv = 9;
 } else pStruct->ssDiv--;

 return 1;

}

void SetCountdTimer(unsigned short hh,unsigned short mn,unsigned short ss,unsigned short ssDiv,CountdTmrStruct *pStruct) {

 pStruct->hh = hh;
 pStruct->mn = mn;
 pStruct->ss = ss;
 pStruct->ssDiv=ssDiv;

}

void StartCountdTimer(CountdTmrStruct *pStruct) {

 pStruct->tmrOn = 1;

}

void StopCountdTimer(CountdTmrStruct *pStruct) {

 pStruct->tmrOn = 0;

}
